-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 23, 2021 at 11:23 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza365`
--

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` int(10) NOT NULL,
  `drink_code` varchar(50) NOT NULL,
  `drink_name` varchar(50) NOT NULL,
  `total_money` int(10) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `drink_code`, `drink_name`, `total_money`, `create_date`, `update_date`) VALUES
(1, 'TRASUA', 'Trà sữa trân châu', 35000, 1629707493, 1629707493),
(2, 'COCA', 'Cocacola', 15000, 1629707493, 1629707493),
(3, 'PEPSI', 'Pepsi', 15000, 1629707493, 1629707493),
(4, 'TRATAC', 'Trà tắc', 20000, 1629707493, 1629707493),
(5, 'FANTA', 'Fanta', 15000, 1629707493, 1629707493);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) NOT NULL,
  `order_code` varchar(50) NOT NULL,
  `pizza_size` varchar(50) NOT NULL,
  `pizza_type` varchar(50) NOT NULL,
  `voucher_id` int(10) NOT NULL,
  `total_money` int(10) NOT NULL,
  `discount` int(10) DEFAULT NULL,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `drink_id` int(10) NOT NULL,
  `status_id` int(10) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_code`, `pizza_size`, `pizza_type`, `voucher_id`, `total_money`, `discount`, `fullname`, `email`, `address`, `drink_id`, `status_id`, `create_date`, `update_date`) VALUES
(1, 'opiz001', 'M', 'Bacon', 1, 200000, 20000, 'Nguyễn Hiếu Nghĩa', 'nghia_nguyen@gmail.com', '77 lê trọng tấn q1 hcm', 1, 1, 1629707493, 1629707493),
(2, 'opiz002', 'S', 'Hawaii', 2, 150000, 15000, 'Hồ Trung Kiên', 'kien_ho@gmail.com', 'ộ nguyễn đình chiểu q1 hcm', 2, 2, 1629707493, 1629707493),
(5, 'opiz003', 'L', 'Seafood', 4, 250000, 50000, 'Trần Đình Nguyên Trang', 'trang_tran@gmail.com', '101 điện biên phủ bình thạnh hcm', 3, 3, 1629707493, 1629707493),
(6, 'opiz004', 'S', 'Bacon', 5, 150000, 5000, 'Phạm Uyên Linh', 'linh_pham@gmail.com', '11 quang trung gò vấp hcm', 4, 4, 1629707493, 1629707493),
(7, 'opiz005', 'M', 'Hawaii', 6, 200000, 80000, 'Nguyễn Văn Toàn', 'toan_nguyen@gmail.com', '20 trần bình trọng q5 hcm', 5, 5, 1629707493, 1629707493);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(10) NOT NULL,
  `status_code` varchar(50) NOT NULL,
  `status_name` varchar(50) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `status_code`, `status_name`, `create_date`, `update_date`) VALUES
(1, 'OPEN', 'open', 1629707493, 1629707493),
(2, 'CONFIRM', 'confirm', 1629707493, 1629707493),
(3, 'CANCEL', 'cancel', 1629707493, 1629707493),
(4, 'DELAY', 'delay', 1629707493, 1629707493),
(5, 'WAIT', 'wait', 1629707493, 1629707493);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `first_name`, `last_name`, `email`, `password`, `create_date`, `update_date`) VALUES
(1, 'hanln', 'Lê Ngọc', 'Hân', 'han_le@gmail.com', 'hanln123', 1629707493, 1629707493),
(2, 'toanpd', 'Phạm Đình', 'Toàn', 'toan_pham@gmail.com', 'toanpd123', 1629707493, 1629707493),
(3, 'hieunt', 'Nguyễn Trọng', 'Hiếu', 'hieu_nguyen@gmail.com', 'hieunt123', 1629707493, 1629707493),
(4, 'thaotn', 'Trần Ngọc', 'Thảo', 'thao_tran@gmail.com', 'thaotn123', 1629707493, 1629707493),
(5, 'toannc', 'Nguyễn Công', 'Toàn', 'toan_nguyen@gmail.com', 'toannc123', 1629707493, 1629707493);

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(10) NOT NULL,
  `voucher_code` varchar(50) NOT NULL,
  `discount` int(10) NOT NULL,
  `is_used_yn` text NOT NULL,
  `create_date` int(10) NOT NULL,
  `update_date` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `voucher_code`, `discount`, `is_used_yn`, `create_date`, `update_date`) VALUES
(1, '12354', 20, 'y', 1629707493, 1629707493),
(2, '12389', 39, 'y', 1629707493, 1629707493),
(4, '13367', 50, 'n', 1629707493, 1629707493),
(5, '10676', 15, 'y', 1629707493, 1629707493),
(6, '10068', 25, 'y', 1629707493, 1629707493);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_drink_code` (`drink_code`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_order_code` (`order_code`),
  ADD KEY `fk_voucher_id` (`voucher_id`),
  ADD KEY `fk_drink_id` (`drink_id`),
  ADD KEY `fk_status_id` (`status_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_status_code` (`status_code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_user_name` (`user_name`),
  ADD UNIQUE KEY `uk_email` (`email`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uk_voucher_code` (`voucher_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `drinks`
--
ALTER TABLE `drinks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_drink_id` FOREIGN KEY (`drink_id`) REFERENCES `drinks` (`id`),
  ADD CONSTRAINT `fk_status_id` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `fk_voucher_id` FOREIGN KEY (`voucher_id`) REFERENCES `vouchers` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
